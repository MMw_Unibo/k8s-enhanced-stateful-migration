# Enhanced stateful migration mechanism integrated within Kubernetes

## Project structure 


**Enhanced-stateful-migration**\
\
├── :open_file_folder:[Enhanced Kubernetes](https://gitlab.com/MMw_Unibo/k8s-enhanced-stateful-migration/-/tree/master/enhanced-kubernetes?ref_type=heads)\
│&emsp;&emsp;├──:open_file_folder:[Enhanced Kubelet](https://gitlab.com/MMw_Unibo/k8s-enhanced-stateful-migration/-/tree/master/enhanced-kubernetes/pkg/kubelet?ref_type=heads)\
│&emsp;&emsp;└──:open_file_folder:[Enhanced Kube-controller manager](https://gitlab.com/MMw_Unibo/k8s-enhanced-stateful-migration/-/tree/master/enhanced-kubernetes/pkg/controller?ref_type=heads)\
│   \
├── :open_file_folder:[DMG mori Anomaly Detection](https://github.com/SimonDahdal/DMGmori_anomaly_detection)\
│\
└── :open_file_folder:[Enhanced Migration controller](https://gitlab.com/MMw_Unibo/k8s-enhanced-stateful-migration/-/blob/master/enhanced-migration-controller/warm_state_controller_with_metrics_collector.py?ref_type=heads)\



