import json
import os
from threading import Thread
from kubernetes import client, config
import subprocess
import statistics
import datetime
import dateutil.tz
import time
import re
import pandas as pd

config.load_kube_config()
v1 = client.CoreV1Api()

source = {
        "metadata": {
            "labels": {
                "type": "source"
            }
        }
}
destination = {
        "metadata": {
            "labels": {
                "type": "destination"
            }
        }
}

user="user"
hot_path= "path_to_source_data_folder"
dpath= "path_to_destination_folder"

cold_path= "path_to_cold_data_destination_folder"


date_format = '%Y-%m-%d %H:%M:%S%z'

def clean_str(ts):
    return re.sub("(\d{6})(\d+)", r"\1", ts).replace("Z", "+00:00")


def execute_command(command):
    '''
        Execute the command and return the outcome
    '''
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    return output, error


def execute_command_in_background(command):
    '''
        Execute the command in background and return the outcome
    '''
    process = subprocess.Popen(command, stdout=None, stderr=None, shell=True)
    return

def copy_file_between_nodes(source_node, destination_node, p_path):
    '''
        Copy the file between the source to the destination node
    '''
    # print("state migration....")
    state_copy = f'ssh {user}{source_node} sshpass -p "Birex2023" rsync -vrpohlg --delete {p_path} {user}{destination_node}:{dpath}'
    delete_source_file = f'ssh {user}{source_node} "rm {p_path}/*"'
    msg, err= execute_command(state_copy)
    if err.decode()!= "":
        print(msg.decode(), err.decode() )
    msg, err=execute_command(delete_source_file)
    if err.decode()!= "":
        print(msg.decode(), err.decode() )


def prepare_dummy_file(size, source_node):
    '''
        Create a dummy file of the specified size
    '''
    if size == -1:
        size = 50
    create_file_command= f'sshpass -p "Birex2023" ssh {user}{source_node} dd if=/dev/zero of={hot_path}/dummy_file.txt bs=1k  count={size}'
    
    msg, err=execute_command(create_file_command)

    if err.decode()!= "":
        print(msg.decode(), err.decode() )


def prepare_LARGE_dummy_file(size, source_node):
    '''
        Create a dummy file of the specified size
    '''
    create_file_command= f'sshpass -p "Birex2023" ssh {user}{source_node} dd if=/dev/zero of={cold_path}/LARGE_dummy.txt bs=1k  count={size}'
    
    msg, err=execute_command(create_file_command)

    if err.decode()!= "":
        print(msg.decode(), err.decode() )



def migrate_application(pod, times_collector, source_node, destination_node, history):
    '''
        Function that manages the migration of an application transparently and independently of the application.

        1. scale the application: the new copy will be scheduled on the node that meets the new affinity conditions
        2. check that the new application is in "container creating"
        3. deactivate the old application
        4. move the state
        5. activate the new application

    '''
    # 1. scale the application
    scale_command = f'kubectl scale --replicas=2 deployment simulation-client -n default'
    msg, err = execute_command(scale_command)
    if err.decode()!= "":
        print(msg.decode(), err.decode() )

    # 2. check if the new app is ready (container ready state) 
    while(len(v1.list_namespaced_pod('default', label_selector="app=simulation-ad").items)<2):
        pass

    history.append(pod.metadata.name)

    for pod_item in v1.list_namespaced_pod('default', label_selector="app=simulation-ad").items:
        if not(pod_item.metadata.name in history):
            new_pod = pod_item
            if (new_pod.status.phase== "Pending"):
                break
            

    while new_pod==None:
        for pod_item in v1.list_namespaced_pod('default', label_selector="app=simulation-ad").items:
            if not(pod_item.metadata.name in history):
                if (pod_item.status.phase== "Pending"):
                    new_pod = pod_item
                    break

    # --> container created
    times_collector.__setitem__("container_created", datetime.datetime.now(dateutil.tz.tzlocal()))

    # # 3. deactivate the old application
    msg, err =execute_command(f'kubectl annotate pod --overwrite {pod.metadata.name} controller.kubernetes.io/pod-deletion-cost=-1')
    if err.decode()!= "":
        print(msg.decode(), err.decode())
    msg, err= execute_command(f'kubectl annotate pod --overwrite {new_pod.metadata.name} controller.kubernetes.io/pod-deletion-cost=40')
    if err.decode()!= "":
        print(msg.decode(), err.decode())

    scale_command = f'kubectl scale --replicas=1 deployment simulation-client -n default'
    msg, err = execute_command(scale_command)
    if err.decode()!= "":
        print(msg.decode(), err.decode() )
    times_collector.__setitem__("downtime_begin", datetime.datetime.now(dateutil.tz.tzlocal()))

    # 4. move the state
    # --> state_migration start
    times_collector.__setitem__("state_migration_start", datetime.datetime.now(dateutil.tz.tzlocal()))
    
    copy_file_between_nodes(source_node, destination_node, hot_path)

    # --> state_migration end
    times_collector.__setitem__("state_migration_end", datetime.datetime.now(dateutil.tz.tzlocal()))

    # 5. activate the new application
    times_collector.__setitem__("signal_sent", datetime.datetime.now(dateutil.tz.tzlocal()))
   
    # 6. check when the new app is active 
    detector_started = True
    opcua_client_started = False
    while not opcua_client_started:
        try:
            api_instance = client.CoreV1Api()
            api_response = api_instance.read_namespaced_pod_log(name=new_pod.metadata.name, namespace='default', timestamps=True)
            if "model" in api_response:
                times_collector.__setitem__("app_name", datetime.datetime.now(dateutil.tz.tzlocal()))
                testo = api_response.split()
                for i, line in enumerate(testo):
                    if line == "model":
                        timestamp = testo[i-1]

                client_started = datetime.datetime.fromisoformat(clean_str(timestamp))
                opcua_client_started = True
                client_started_naive = client_started.replace(tzinfo=None)
                times_collector.__setitem__("app_name", client_started)

        except client.rest.ApiException as e:
            # print('Found exception in reading logs', e) 
            pass
    
    # --> fine downtime
    # times_collector.__setitem__("downtime_end", client_started)
    times_collector.__setitem__("downtime_end", datetime.datetime.now(dateutil.tz.tzlocal()))

    print(str(times_collector["app_name"] - client_started))
    # print("fine")

def main():

    # sizes = [4, 5, 10, 15, 20, 30, 40, 50, 100, 150, 200, 300, 400, 500, 750, 1000, 1500, 2000, 3000, 4000, 5000, 10000, 15000, 20000, 30000, 50000, 100000, 150000, 200000, 300000, 400000, 500000 ]
    # sizes = [40000, 60000, 70000, 80000,90000, 250000, 600000, 700000, 800000, 900000, 1000000 ]
    sizes = [4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152,  ] # LOGARITMINCA BASE 2

    for i, size in enumerate(sizes):
        print("*************** Round: "+str(i)+" ***************")
        avg_time = []
        avg_downtime = []
        avg_data_transfer_time = [] 
        history = []
        avg_signal_for_creation = []
        avg_image_pulled= []
        avg_container_created= []
        avg_downtime_start= []
        avg_data_migration_start= []
        avg_data_migration_end= []
        avg_unlock= []
        avg_start_container= []
        avg_start_app= []
        avg_cold_data_migration_start = []
        avg_cold_data_migration_end = []

        node_list = v1.list_node()
        
        for n in node_list.items:
                if (('type', 'source') in n.metadata.labels.items()):
                    source_node = n.status.addresses[0].address
                    try:
                        v1.patch_node(n.metadata.name, source)
                    except Exception as e:
                        print(e)

        prepare_dummy_file(50, source_node)
        ls_command= f'ssh {user}{source_node} du -h {hot_path}/dummy_file.txt'
    
        msg, err=execute_command(ls_command)

        prepare_LARGE_dummy_file(size, source_node)
        ls_command= f'ssh {user}{source_node} du -h {cold_path}/LARGE_dummy.txt'
    
        msg, err=execute_command(ls_command)

        if err.decode()!= "":
            print(msg.decode(), err.decode() )

        for j in range(0,33):
            print("######### attempt ", j+1, " LARGE file size: ", size, "k ########")
            node_list = v1.list_node()
            times_collector= {}
            # change node labels: node affinity changes
            for n in node_list.items:
                if (('type', 'source') in n.metadata.labels.items()):
                    source_node = n.status.addresses[0].address
                    try:
                        v1.patch_node(n.metadata.name, destination)
                    except Exception as e:
                        print(e)
                elif (('type', 'destination') in n.metadata.labels.items()):
                    destination_node = n.status.addresses[0].address
                    try:
                        v1.patch_node(n.metadata.name, source)
                    except Exception as e:
                        print(e)

            pod_list = v1.list_namespaced_pod('default', label_selector="app=simulation-ad")
            ok = False
            for pod in pod_list.items:
                if("simulation-client" in pod.metadata.name):
                    ok = True
                    break
                    # pass
            if not ok: 
                print("pod not found. ")
                exit(-1)

            # --> start migration
            migration_start = datetime.datetime.now(dateutil.tz.tzlocal())
            migrate_application(pod, times_collector, source_node, destination_node, history)

            cold_data_migration_start_abs = datetime.datetime.now(dateutil.tz.tzlocal())
            copy_file_between_nodes(source_node, destination_node, cold_path)
            cold_data_migration_end_abs = datetime.datetime.now(dateutil.tz.tzlocal())

            # --> end migrazione
            migration_end = datetime.datetime.now(dateutil.tz.tzlocal())


            msg, err = execute_command(f'scp {user}{destination_node}:/test.json .')
            if err.decode() != "":
                print(msg, err)
            decoder = json.JSONDecoder()
            with open("test.json") as file:
                data = file.read()
                new_data, _ = decoder.raw_decode(data)
           
            new_data_signal_received = datetime.datetime.fromisoformat(clean_str(new_data["SignalReceived"]))
            new_data_image_pulled = datetime.datetime.fromisoformat(clean_str(new_data["ImagePulled"]))
            new_data_container_created = datetime.datetime.fromisoformat(clean_str(new_data["ContainerCreated"]))
            new_data_container_started = datetime.datetime.fromisoformat(clean_str(new_data["ContainerStarted"]))
            new_data_unlock = datetime.datetime.fromisoformat(clean_str(new_data["Unlock"]))
            
            start = migration_start
            signal_for_creation = new_data_signal_received - start
            image_pulled = new_data_image_pulled - start
            container_created = new_data_container_created - start
            downtime_start = (times_collector["downtime_begin"]) - start
            data_migration_start = (times_collector["state_migration_start"]) - start
            data_migration_end = (times_collector["state_migration_end"])- start
            unlock = new_data_unlock - start
            start_container = new_data_container_started - start
            start_app = times_collector["downtime_end"]- start
            cold_data_migration_start = cold_data_migration_start_abs -start
            cold_data_migration_end = cold_data_migration_end_abs -start

            avg_signal_for_creation.append(float(str(signal_for_creation).split(":")[2]))
            avg_image_pulled.append(float(str(image_pulled).split(":")[2]))
            avg_container_created.append(float(str(container_created).split(":")[2]))
            avg_downtime_start.append(float(str(downtime_start).split(":")[2]))
            avg_data_migration_start.append(float(str(data_migration_start).split(":")[2]))
            avg_data_migration_end.append(float(str(data_migration_end).split(":")[2]))
            avg_unlock.append(float(str(unlock).split(":")[2]))
            avg_start_container.append(float(str(start_container).split(":")[2]))
            avg_start_app.append(float(str(start_app).split(":")[2]))

            avg_cold_data_migration_start.append(float(str(cold_data_migration_start).split(":")[2]))
            avg_cold_data_migration_end.append(float(str(cold_data_migration_end).split(":")[2]))

            container_creation = times_collector["container_created"] - migration_start

            tot_time = migration_end - migration_start
            data_migration_time = times_collector["state_migration_end"] - times_collector["state_migration_start"]
            downtime = times_collector["downtime_end"] - times_collector["downtime_begin"]
            app_activation = times_collector["downtime_end"] - new_data_container_started 
            container_activation = new_data_container_started - new_data_unlock

            print("Total time: "+str(tot_time))
            print("State migration: "+str(data_migration_time))
            print("Downtime: "+str(downtime))
            print("container activation: "+str(container_activation))
            print("app activation: "+str(app_activation))

            avg_time.append(float(str(tot_time).split(":")[2]))
            avg_downtime.append(float(str(downtime).split(":")[2]))
            avg_data_transfer_time.append(float(str(data_migration_time).split(":")[2]))

            time.sleep(2)
            
        avg_time_mean = statistics.mean(avg_time)
        avg_downtime_mean = statistics.mean(avg_downtime)
        avg_data_transfer_time_mean = statistics.mean(avg_data_transfer_time)
        avg_signal_for_creation_mean = statistics.mean(avg_signal_for_creation)
        avg_image_pulled_mean = statistics.mean(avg_image_pulled)
        avg_container_created_mean = statistics.mean(avg_container_created)
        avg_downtime_start_mean = statistics.mean(avg_downtime_start)
        avg_data_migration_start_mean = statistics.mean(avg_data_migration_start)
        avg_data_migration_end_mean = statistics.mean(avg_data_migration_end)
        avg_unlock_mean = statistics.mean(avg_unlock)
        avg_start_container_mean = statistics.mean(avg_start_container)
        avg_start_app_mean = statistics.mean(avg_start_app)
        avg_cold_data_migration_start_mean = statistics.mean(avg_cold_data_migration_start)
        avg_cold_data_migration_end_mean = statistics.mean(avg_cold_data_migration_end)

        fp1 = open("reports/downtimeReport.txt", "a")
        fp2 = open("reports/totalTimeReport.txt", "a")
        fp3 = open("reports/dataTransferReport.txt", "a")
        fp4 = open("reports/times.txt", "a")

        fp1.write(str(avg_downtime_mean)+",\n")
        fp2.write(str(avg_time_mean)+",\n")
        fp3.write(str(avg_data_transfer_time_mean)+",\n")
        fp4.write(str(start)+","+
                  str(avg_signal_for_creation_mean)+","+
                  str(avg_image_pulled_mean)+","+
                  str(avg_container_created_mean)+","+
                  str(avg_downtime_start_mean)+","+
                  str(avg_data_migration_start_mean)+","+
                  str(avg_data_migration_end_mean)+","+
                  str(avg_unlock_mean)+","+
                  str(avg_start_container_mean)+","+
                  str(avg_start_app_mean)+","+
                  str(avg_cold_data_migration_start_mean)+","+
                  str(avg_cold_data_migration_end_mean)+",\n")
        
        fp1.close()    
        fp2.close()
        fp3.close()
        fp4.close()
        


if __name__ == "__main__":
    main()
