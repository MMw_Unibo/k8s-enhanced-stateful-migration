# Enhanced stateful migration mechanism integrated within Kubernetes
K8s lacks native support for service migration, which is crucial in environments where production appliances generate vast amounts of data, potentially saturating industrial edge nodes, and essential for applications.

## Enhanced mechanism design

With this mechanism, we aim to shift migration time cosuming operations to periods outside the downtime phase. To do this, we introduce a proactive preparation phase before service migration, where the target node prepares the execution environment and acquires the necessary resources. For example, in this phase, the application image is downloaded while the service on the source node remains operational. Migration begins only when the target node is ready.

To minimize application data transfer time, we separate the application state into a **Hot State** and a **Cold State**. The Hot State, limited in size, represents the last data collected by the application, it is essential for real-time service operation and for calculating the current and future state of the ML-Driven service. It is transferred during downtime. The Cold State, considerable size, represents all the data collected by the application so far, and it is transferred asynchronously after service reactivation on the destination node. This approach ensures faster service reactivation and predictable downtime, regardless of the total state size.

In an industrial context, the Hot State typically consists of the most recent production data. 




### Enhanced K8s Kublet
The first step to optimize the the migration mechanism is to modify the K8s kublet code on the destination node for making it capable of performing all the preparation actions like, service image acquisition, resource allocation, and container creation in advance. We modify the kublet code by introducing a new container state, **container ready**, between the service image download and service startup. This modification is needed to allow the Kublet to download the service image, to prepare the container in advance and to prevent the service startup by stopping the process in \textit{container ready}. Then we trigger the process reactivation as soon as the state is available on the node. It is possible to chose other reactivation triggers according different policies.

### Enhanced K8s Kube-controller-manager
Kube-controller-manager handles the other part of the migration mechanism, operating on the master node of the cluster. Specifically, the controller is responsible for initiating the preparatory phase on the target node, deactivating the application on the source node, and then initiating the migration of state. We opted to utilize the **replicaset** configuration to execute the preparation request command. Following the adjustment of the node affinity specification, the process involves increasing the number of replicas in the application configuration. Subsequently, the controller consults the scheduler, which assigns the destination node as the host for the additional copy of the service. The enhanced Kubelet, previously described, prepare the service on the destination node. As soon as the container state is on **container ready**, the controller proceeds to deactivate the original application and starts the state migration. The container on the source node is then deleted to ensure only one application persists within the cluster, achieved by reducing the number of replicas to 1.


### Cold and Hot State Management 

The enhanced application migration method involves implementing a state management mechanism that distinguishes between data required for real-time operation and data that can be accessed with less stringent time constraints. The Hot State is immediately moved on the destination node, and it has very reduced size compared to the full state. In this way, our proposed enhanced migration mechanism is able to reactivate the service as soon as possible by reducing the downtime drastically. Then, the Cold State can be managed asynchronously in a subsequent migration phase following service reactivation, in background.
